﻿using Otter;
using System;

namespace ONEGAM
{
    internal class MenuScene : Scene
    {
        public MenuScene() : base() { }

        public override void Begin()
        {
            base.Begin();

            Console.WriteLine("Entered MenuScene");
        }
    }
}
