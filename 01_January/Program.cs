﻿using Otter;

namespace ONEGAM
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game("@Thunraz #1GAM - January", 160, 140, fullscreen: false);
            game.SetWindow(160, 140, fullscreen: false, vsync: true);
            game.SetWindowScale(2);

            Global.PlayerSession = game.AddSession("PlayerSession");

            Global.PlayerSession.Controller.Up.AddKey(Key.W);
            Global.PlayerSession.Controller.Left.AddKey(Key.A);
            Global.PlayerSession.Controller.Right.AddKey(Key.D);
            Global.PlayerSession.Controller.A.AddKey(Key.Space);
            Global.PlayerSession.Controller.Select.AddKey(Key.R);

            game.FirstScene = new GameScene(game.Width, game.Height);
            Music gameMusic = new Music("Assets/2014-01-22_FixJanuary_V1.0.ogg");
            gameMusic.Volume = 25.0f;
            gameMusic.Play();


            game.Start();
        }
    }
}
