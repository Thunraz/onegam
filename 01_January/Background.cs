﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otter;

namespace ONEGAM
{
    public class Background : Entity
    {
        Otter.Image _backgroundImage;

        private System.Collections.Generic.List<Graphic> _backgroundObjects = new List<Graphic>();
        private System.Collections.Generic.List<Graphic> _backgroundObjects2 = new List<Graphic>();

       
        public void SetXYPosition ( float x, float y)
        {
            float dx = X - x;
            float dy = Y - y;
            X = x;
            Y = y;
            foreach (var i in _backgroundObjects)
            {
                i.X += 0.25f * dx;
                i.Y += 0.25f * dy;
            }
            foreach (var i in _backgroundObjects2)
            {
                i.X += 0.5f * dx;
                i.Y += 0.5f * dy;
            }
        }

        public Background(int width, int height)
        {
            int xMax = 160;
            int yMax = 140;

            _backgroundImage = Image.CreateRectangle(width, height, Global.Color8);

            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            CreateInitialBackGroundObject();
            this.Layer = 1000;
        }

        /// <summary>
        /// Called during the update of the game.
        /// </summary>
        public override void Update()
        {
            base.Update();
            // Reset Graphics
            SetGraphic(_backgroundImage);

           // Console.WriteLine("2 " + _backgroundObjects2.Count);
           // Console.WriteLine("1 " + _backgroundObjects.Count);

            bool CreateNew = false;
            System.Collections.Generic.List<Graphic> newList = new List<Otter.Graphic>();
            System.Collections.Generic.List<Graphic> newList2 = new List<Otter.Graphic>();
            
            foreach (var i in _backgroundObjects)
            {
                if (i.X + i.Width <= 0)
                {
                   // Console.WriteLine(" Killing ");
                    CreateNew = true;
                }
                else
                {
                    newList.Add(i);
                    AddGraphic(i);
                }
            }

            foreach (var i2 in _backgroundObjects2)
            {
                if (i2.X + i2.Width <= 0)
                {
                    //Console.WriteLine(" Killing ");
                    CreateNew = true;
                }
                else
                {
                    newList2.Add(i2);
                    AddGraphic(i2);
                }
                
            }
            _backgroundObjects = newList;
            _backgroundObjects2 = newList2;

            if (CreateNew)
            {
                CreateNewBackGroundObject();
            }
        }

        private void CreateNewBackGroundObject()
        {
            if (_backgroundObjects.Count< _backgroundObjects2.Count)
            {
                Image bgImage = Image.CreateRectangle(Rand.Int(10, 35), 300, Global.Color9);
                bgImage.X = Otter.Rand.Float(160, 180);
                bgImage.Y = 30 + Rand.Int(-10, +10);
                _backgroundObjects.Add(bgImage);
                //Console.WriteLine("1 " + bgImage.X);
            }
            else
            {
                Image bgImage = Image.CreateRectangle(Rand.Int(5, 30), 300, Global.Color12);
                bgImage.X = Otter.Rand.Float(160, 180);
                bgImage.Y = 80 + Rand.Int(-10, +10);
                _backgroundObjects2.Add(bgImage);
            }
        }

        private void CreateInitialBackGroundObject()
        {
            if (_backgroundObjects.Count < _backgroundObjects2.Count)
            {
                Image bgImage = Image.CreateRectangle(Rand.Int(10, 35), 300, Global.Color9);
                bgImage.X = Otter.Rand.Float(-5, 240);
                bgImage.Y = 30 + Rand.Int(-10, +10);
                _backgroundObjects.Add(bgImage);
            }
            else
            {
                Image bgImage = Image.CreateRectangle(Rand.Int(5, 30), 300, Global.Color12);
                bgImage.X = Otter.Rand.Float(-5, 160);
                bgImage.Y = 80 + Rand.Int(-10, +10);
                _backgroundObjects2.Add(bgImage);
            }
        }
        
    }
}
