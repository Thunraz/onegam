﻿using Otter;

namespace ONEGAM
{
    internal class Projectile : Entity
    {
        Image _image = Image.CreateRectangle(2, Color.Cyan);
        Orientation _orientation;

        float ySpeed;

        public Projectile(float x, float y, Orientation orientation)
            : base(x, y)
        {
            SetGraphic(_image);
            SetHitbox(_image.Width, _image.Height, (int)Tag.PROJECTILE);

            _image.CenterOrigin();
            Collider.CenterOrigin();

            _orientation = orientation;

            LifeSpan = 600;

            ySpeed = Rand.Float(-0.8f, 0.8f);
        }

        public override void Update()
        {
            base.Update();

            Y += ySpeed * Global.TimeFactor * Game.DeltaTime;
            if (_orientation == Orientation.LEFT)
            {
                X -= 10.0f * Global.TimeFactor * Game.DeltaTime;
            }
            else
            {
                X += 10.0f * Global.TimeFactor * Game.DeltaTime;
            }
        }
    }
}
