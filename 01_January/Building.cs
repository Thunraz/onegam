﻿using Otter;

namespace ONEGAM
{
    public class Building : Entity
    {
        Image _image;
        bool _hasSpawnedBuilding = false;
        private bool _first;

        public int Width { get { return _image.Width; } }

        public Building(float x, float y, bool first = false)
            : base(x, y)
        {
            _first = first;
            var blocksInSuccession = Rand.Int(Global.MinBlocksInSuccession, Global.MaxBlocksInSuccession);
            blocksInSuccession *= Global.BlockSize * (first ? 3 : 1);

            var gradient = new Gradient(
                blocksInSuccession,
                Global.BlocksDown * Global.BlockSize,
                Global.Color3,
                Global.Color3,
                Global.Color12,
                Global.Color12
            );
            _image = gradient;

            SetGraphic(_image);
            SetHitbox(_image.Width, _image.Height, (int)Tag.BUILDING);

            X = x;
            Y = y;
        }

        public override void Update()
        {
            base.Update();

            if (X <= Scene.CameraX + Scene.Width && !_hasSpawnedBuilding && !_first)
            {
                _hasSpawnedBuilding = true;

                var xDistance = Rand.Int(Global.MinBuildingDistance, Global.MaxBuildingDistance);
                var yDistance = Rand.Int(Global.MinBuildingYDifference, Global.MaxBuildingYDifference);

                if (Y + yDistance * Global.BlockSize >= 100.0f)
                {
                    yDistance = -Global.MaxBuildingYDifference;
                }

                var next = new Building(
                    X + Width + xDistance * Global.BlockSize,
                    Y + yDistance * Global.BlockSize
                );

                Scene.Add(next);
            }
            else if (X + Width < Scene.CameraX)
            {
                RemoveSelf();
            }
        }
    }
}
