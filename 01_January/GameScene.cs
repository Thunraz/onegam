﻿using Otter;
using System;

namespace ONEGAM
{
    internal class GameScene : Scene
    {
        Player _player = new Player(Global.PlayerSession);
        Shader _timeWarpShader;
        Background _background;
        Score _score;


        float _enemySpawnTimer = Global.EnemySpawnTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScene"/> class.
        /// </summary>
        public GameScene(int width, int height)
            : base(width, height)
        {
            _score = new Score();
            Add(_player);
            _background = new Background(200, 200);
            Add(_background);
            _timeWarpShader = new Shader("Assets/edge.frag");
            Game.Instance.Surface.AddShader(_timeWarpShader);

            var firstBuilding = new Building(-16, 32, true);
            var nextBuilding = new Building(
                firstBuilding.X + firstBuilding.Width + Rand.Int(Global.MinBuildingDistance, Global.MaxBuildingDistance) * Global.BlockSize,
                firstBuilding.Y
            );

            Add(firstBuilding);
            Add(nextBuilding);
            Add(_score);


            CameraX = 0.0f;
            CameraY = 0.0f;
        }

        /// <summary>
        /// The main update loop of the scene.
        /// </summary>
        public override void Update()
        {
            if (_enemySpawnTimer > 0)
            {
                _enemySpawnTimer -= Game.DeltaTime * Global.TimeFactor;
            }
            else
            {
                SpawnEnemy();
            }


            base.Update();

            _background.SetXYPosition(CameraX, CameraY - 20);
            _score.SetXYPosition(CameraX, CameraY);

            if (_player.Y <= 120.0f)
            {
                CenterCamera(_player.X, _player.Y - Game.Instance.HalfHeight / 2);
            }

            if (_player.Y > CameraY + Height || _player.IsDead)
            {
                Game.Instance.SwitchScene(new GameOverScene(Width, Height));
                return;
            }



            _score.Update();

            _timeWarpShader.SetParameter("timer", Timer * (_player.LooksBack ? 5.0f : 0.0f));

            Global.Score = (int)(Timer + Global.TimeFactor);


            if (Global.PlayerSession.Controller.Select.Down)
            {
                Console.WriteLine("Reset!");
                Game.Instance.SwitchScene(new GameScene(Width, Height));
                return;
            }
        }

        private void SpawnEnemy()
        {
            //Console.WriteLine("Spawning");
            _enemySpawnTimer += Global.EnemySpawnTimer;
            Add(new Enemy(CameraX + 20, _player.Y - 24));
        }
    }
}
