﻿
using Otter;
namespace ONEGAM
{
    internal class Global
    {
        public static Session PlayerSession;

        public static float EnemyMovementSpeedFactor = 1.1f;

        public static float MaxHorizontalSpeed = 2.0f;
        public static float MoveSpeedIncrement = 0.5f;
        public static float MaxVerticalSpeed = 2.0f;
        public static float FallingSpeedIncrement = 0.1f;
        public static float MovementIncreaseFactor = 2.0f;
        public static float MovementDecreaseFactor = 0.5f;

        public static float ShootDelay = 6.0f;

        public static float EnemySpawnTimer = 60.0f;

        public static int Score = 0;
        public static float TimeFactor = 1.0f;

        public static float RemainingBulletTime = 100.0f;
        public static float MaxBulletTime = 100.0f;
        public static float BulletTimeDrain = 0.5f; // when looking left
        public static float BulletTimeRefill = 0.7f;

        public static Color[] ColorArray = new Color[] { Color1, Color2, Color3, Color4, Color5, Color6, Color7, Color8, Color9, Color10, Color11, Color12 };

        public static Color GetRandomColor()
        {
            return ColorArray[Rand.Int(ColorArray.Length - 1)];
        }

        public static Color Color1
        {
            get { return new Color(153.0f / 255.0f, 255.0f / 255.0f, 204.0f / 255.0f); }
        }
        public static Color Color2
        {
            get { return new Color(139.0f / 255.0f, 232.0f / 255.0f, 199.0f / 255.0f); }
        }
        public static Color Color3
        {
            get { return new Color(125.0f / 255.0f, 209.0f / 255.0f, 195.0f / 255.0f); }
        }
        public static Color Color4
        {
            get { return new Color(111.0f / 255.0f, 185.0f / 255.0f, 190.0f / 255.0f); }
        }
        public static Color Color5
        {
            get { return new Color(97.0f / 255.0f, 162.0f / 255.0f, 185.0f / 255.0f); }
        }
        public static Color Color6
        {
            get { return new Color(83.0f / 255.0f, 139.0f / 255.0f, 181.0f / 255.0f); }
        }
        public static Color Color7
        {
            get { return new Color(70.0f / 255.0f, 116.0f / 255.0f, 176.0f / 255.0f); }
        }
        public static Color Color8
        {
            get { return new Color(56.0f / 255.0f, 93.0f / 255.0f, 172.0f / 255.0f); }
        }
        public static Color Color9
        {
            get { return new Color(42.0f / 255.0f, 70.0f / 255.0f, 167.0f / 255.0f); }
        }
        public static Color Color10
        {
            get { return new Color(28.0f / 255.0f, 46.0f / 255.0f, 162.0f / 255.0f); }
        }
        public static Color Color11
        {
            get { return new Color(14.0f / 255.0f, 23.0f / 255.0f, 158.0f / 255.0f); }
        }
        public static Color Color12
        {
            get { return new Color(0, 0, 153.0f / 255.0f); }
        }

        public static int MinBuildingDistance = 2;
        public static int MaxBuildingDistance = 8;
        public static int BlockSize = 8;
        public static int BlocksDown = 20;
        public static int MinBlocksInSuccession = 6;
        public static int MaxBlocksInSuccession = 12;
        public static int MinBuildingYDifference = -2;
        public static int MaxBuildingYDifference = 3;

        internal class TIMEFACTOR
        {
            public const float SLOW = 0.20f, NORMAL = 0.9f, FAST = 1.25f;

        }
    }

}
