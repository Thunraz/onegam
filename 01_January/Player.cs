﻿using Otter;
using System;

namespace ONEGAM
{
    internal class Player : Entity
    {
        Session _player;
        Speed _speed = new Speed(0, 0, Global.MaxHorizontalSpeed, Global.MaxVerticalSpeed, true);
        Orientation _orientation;
        bool _isOnGround;
        float _shootDelay = Global.ShootDelay;
        private int _width = 8;
        private int _height = 16;
        public bool LooksBack { get { return _orientation == Orientation.LEFT; } }

        private float _playerTrailTimer = 0.0f;
        private float _playerTrailTimerMax =7.0f;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="player">The player.</param>
        public Player(Session player)
            : base()
        {
            // Base color
            var image = Image.CreateRectangle(_width, _height, Global.Color1);
            SetGraphic(image);


            Global.RemainingBulletTime = Global.MaxBulletTime;

            _player = player;
            IsDead = false;

            SetHitbox(_width, _height, (int)Tag.PLAYER);

            image.CenterOrigin();
            Collider.CenterOrigin();

            _speed.X = 0.0f; // Global.MaxHorizontalSpeed;
            _speed.Y = 0.0f;

            X = 0;
            Y = 0;
            _isOnGround = false;
            _orientation = Orientation.RIGHT;
        }

        /// <summary>
        /// Called during the update of the game.
        /// </summary>
        public override void Update()
        {
            base.Update();

            #region Movement

            // Don't allow the player to move left/right

            if (_speed.X <= 0.2f)
            {
                _speed.X = 0.2f;
            }
            if (_isOnGround)
            {
                _speed.X *= 1.25f;
            }

            // But allow turning backward/forward
            if (_player.Controller.Left.Down)
            {

                _orientation = Orientation.LEFT;

                //// Slow time if he's in the air
                //if (!_isOnGround)
                //{
                //    Global.TimeFactor = Global.TIMEFACTOR.SLOW;
                //}
            }

            if (_player.Controller.Right.Down || (_player.Controller.Right.Down && _player.Controller.Left.Down))
            {
                _orientation = Orientation.RIGHT;
            }


            if (_player.Controller.Up.Down && _isOnGround)
            {
                _speed.Y -= Global.MaxVerticalSpeed;
            }

            //if (Global.TimeFactor == Global.TIMEFACTOR.SLOW && _isOnGround)
            //{
            //    Global.TimeFactor = Global.TIMEFACTOR.NORMAL;
            //}

            #endregion Movement


            #region BulletTime
            if (_orientation == Orientation.RIGHT)
            {
                Global.RemainingBulletTime += Global.BulletTimeRefill * Game.DeltaTime;
                if (Global.RemainingBulletTime >= Global.MaxBulletTime)
                {
                    Global.RemainingBulletTime = Global.MaxBulletTime;
                }

                Global.TimeFactor = Global.TIMEFACTOR.NORMAL;
            }
            else if (_orientation == Orientation.LEFT)
            {
                Global.RemainingBulletTime -= Global.BulletTimeDrain * Game.DeltaTime;
                if (Global.RemainingBulletTime <= 0.0f)
                {
                    _orientation = Orientation.RIGHT;
                }

                Global.TimeFactor = Global.TIMEFACTOR.SLOW;
            }


            #endregion

            #region Collision

            bool xCollision = false, yCollision = false;

            // Check for x collision
            var c = Collider.Collide(X + _speed.X, Y, (int)Tag.BUILDING);

            if (c != null)
            {
                xCollision = true;
            }

            c = Collider.Collide(X, Y + _speed.Y, (int)Tag.BUILDING);
            if (c != null)
            {
                yCollision = true;
            }

            c = Collider.Collide(X, Y + _speed.Y, (int)Tag.ENEMY);
            if (c != null)
            {
                IsDead = true;
            }

            #endregion Collision

            #region Do the actual movement (and gravity)

            if (xCollision)
            {
                _speed.X = 0.0f;
            }
            else
            {
                X += _speed.X * Global.TimeFactor * Game.DeltaTime;
            }

            if (yCollision)
            {
                _speed.Y = 0.0f;
                Y -= 0.1f;
                _isOnGround = true;
            }
            else
            {
                Y += _speed.Y * Global.TimeFactor * Game.DeltaTime;
                _isOnGround = false;
            }

            // Gravity
            if (!_isOnGround)
            {
                _speed.Y += Global.FallingSpeedIncrement * Global.TimeFactor * Game.DeltaTime;
            }

            #endregion Do the actual movement (and gravity)

            #region Shoot!

            if (_shootDelay >= 0)
            {
                _shootDelay -= Game.DeltaTime * Global.TimeFactor;
            }

            if (_player.Controller.A.Down && _shootDelay <= 0)
            {
                _shootDelay += Global.ShootDelay;

                Console.WriteLine("Shoot!");
                Scene.Add(new Projectile(X + _width, Y + 4, _orientation));
            }

            #endregion Shoot!

            #region  Trail
            if (_playerTrailTimer >= 0.0f)
            {
                _playerTrailTimer -= Game.DeltaTime;
            }
            else
            {
                Scene.Add(new PlayerTrail(X, Y));
                _playerTrailTimer = _playerTrailTimerMax;
            }
            
            #endregion Trail

            //if (Timer % (3 / Global.TimeFactor) == 0)
            //{
            //    Scene.Add(new PlayerTrail(X, Y));
            //}
        }

        public bool IsDead { get; private set; }
    }
}
