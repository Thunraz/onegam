﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otter;

namespace ONEGAM
{
    public class Score : Entity
    {
        private Image _bulletTimeImage = Image.CreateRectangle(Game.Instance.HalfWidth, 8, Global.Color1);
        private Text _score;
        public Score ()
        {
            _score = new Text(Global.Score.ToString(), size: 12, font: "Assets/Minecraftia.ttf");
            _score.Smooth = false;
            AddGraphic(_score);

            AddGraphic(_bulletTimeImage);
            _bulletTimeImage.X -= Game.Instance.Width;
            _bulletTimeImage.Y = -5;

            Layer = 99;
        }


        public override void Update ()
        {
            _score.String = Global.Score.ToString();

            _bulletTimeImage.ScaleX = Global.RemainingBulletTime / Global.MaxBulletTime;
        }

        internal void SetXYPosition(float x, float y)
        {
            X = x + Game.Instance.Width - _score.Width;
            //X = x;
            Y = y + 4;
        }
    }
}
