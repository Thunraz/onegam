﻿using Otter;
using System;

namespace ONEGAM
{
    internal class PlayerTrail : Entity
    {
        Image _image = Image.CreateRectangle(8, 16);

        public PlayerTrail(float x, float y)
            : base(x, y)
        {
            SetGraphic(_image);
            _image.CenterOrigin();

            LifeSpan = 100;
            Layer = 100;
        }

        public override void Update()
        {
            base.Update();

            if (Global.TimeFactor == Global.TIMEFACTOR.SLOW)
            {
                LifeSpan += 4;
            }

            if ((LifeSpan - Timer) % 20 == 0)
            {
                _image.Color = Global.GetRandomColor();
            }

            _image.Alpha = Util.ScaleClamp(Timer, 0.5f, LifeSpan, 1.0f, -5.0f);
        }
    }
}
