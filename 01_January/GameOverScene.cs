﻿using Otter;
using System;

namespace ONEGAM
{
    public class GameOverScene : Scene
    {
        public GameOverScene(int width, int height)
            : base(width, height)
        {
            var gameOver = new Text("GAME OVER!", size: 14, font: "Assets/Minecraftia.ttf");
            gameOver.Smooth = false;
            gameOver.X = HalfWidth - gameOver.HalfWidth;
            gameOver.Y = HalfHeight - gameOver.HalfHeight;
            AddGraphic(gameOver);

            var scoreText = new Text(
                string.Format("Final score: {0}", Global.Score),
                size: 10,
                font: "Assets/Minecraftia.ttf"
            );
            scoreText.X = HalfWidth - scoreText.HalfWidth;
            scoreText.Y = gameOver.Y + gameOver.Height + 10;
            AddGraphic(scoreText);
        }

        public override void Update()
        {
            base.Update();
            
            if (Global.PlayerSession.Controller.Select.Down)
            {
                Console.WriteLine("Reset!");
                Game.Instance.SwitchScene(new GameScene(Width, Height));
                return;
            }
        }
    }
}
