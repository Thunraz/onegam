﻿uniform sampler2D texture;
 
uniform float timer;
 
void main() {

	if(timer == 0.0)
	{
		gl_FragColor = texture2D(texture, gl_TexCoord[0].xy);
	}
	else
	{
		vec2 coord = gl_TexCoord[0].xy;

		coord.x += sin(radians(timer)) * 0.005;
		coord.y += cos(radians(timer)) * 0.005;

		vec4 pixel_color = texture2D(texture, coord);
 
		gl_FragColor = pixel_color;
	}
}