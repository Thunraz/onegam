﻿using Otter;
using System;

namespace ONEGAM
{
    internal class Enemy : Entity
    {
        Image _image = Image.CreateRectangle(8, 16, Color.Red);
        Speed _speed = new Speed(0, 0, Global.MaxHorizontalSpeed * Global.EnemyMovementSpeedFactor, Global.MaxVerticalSpeed * 5*  Global.EnemyMovementSpeedFactor, true);
        bool _isOnGround;
        bool _wasOnGroundLastFrame;

        public Enemy(float x, float y)
            : base(x, y)
        {
            SetGraphic(_image);
            SetHitbox(8, 16, (int)Tag.ENEMY);

            _image.CenterOrigin();
            Collider.CenterOrigin();

            _speed.X = 1.2f;
            _speed.Y = 0.0f;
            _isOnGround = false;
        }

        public override void Update()
        {
            base.Update();

            #region Collision

            bool xCollision = false, yCollision = false;

            // Check for x collision
            var c = Collider.Collide(X + _speed.X, Y, (int)Tag.BUILDING);

            if (c != null)
            {
                xCollision = true;
            }

            c = Collider.Collide(X, Y + _speed.Y, (int)Tag.BUILDING);
            if (c != null)
            {
                yCollision = true;
            }

            // Check for collision with projectile
            c = Collider.Collide(X, Y, (int)Tag.PROJECTILE);
            if (c != null)
            {
                Console.WriteLine("Enemy hit!");
                _image.Scale = 1.1f;
                _image.Color = Color.White;
                Tween(_image, new { ScaleX = 1, ScaleY = 1 }, 60);
                Tween(_image.Color, new { R = Color.Red.R, B = Color.Red.B, G = Color.Red.G }, 60);
                _speed.X = -10.0f;
                X -= 5.0f;
                Global.RemainingBulletTime += 25.0f;
                c.Entity.RemoveSelf();
            }

            #endregion Collision

            if (Y > Scene.CameraY + Scene.Height)
            {
                //Console.WriteLine("Removing");
                RemoveSelf();
            }
            if (X < Scene.CameraX - 100)
            {
                RemoveSelf();
            }

            #region Do the actual movement (and gravity)

            if (xCollision)
            {
                _speed.X = 0.0f;
            }
            else
            {
                X += _speed.X * Global.TimeFactor * Game.DeltaTime;
            }

            if (yCollision)
            {
                _speed.Y = 0.0f;
                _isOnGround = true;
            }
            else
            {
                Y += _speed.Y * Global.TimeFactor * Game.DeltaTime;
                _isOnGround = false;
            }

            // Gravity
            if (!_isOnGround)
            {
                if (_wasOnGroundLastFrame )
                {
                    _speed.Y -= Global.MaxVerticalSpeed * Rand.Float(0.5f, 1.4f);
                }

                _speed.Y += Global.FallingSpeedIncrement * Global.TimeFactor * Game.DeltaTime;
                if (_speed.X <= 0.1f)
                {
                    _speed.X = 0.1f;
                }

                _speed.X *= 1.25f;
               
            }

            _wasOnGroundLastFrame = _isOnGround;
            #endregion Do the actual movement (and gravity)
        }
    }
}
