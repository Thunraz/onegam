﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.Window;

namespace JamUtilities
{
    class ShapeParticle : IParticle
    {

        public ShapeParticle(Shape shape, float lifeTime, Vector2f position, Vector2f velocity)
        {
            SetShape(shape);
            InitialAlpha = shape.FillColor.A;
            RemainingLifeTime =  TotalLifeTime = lifeTime;
            Position = position;
            Velocity = velocity;
            IsAlive = true;
            IsImmortal = false;
            AlphaChangeType = PennerDoubleAnimation.EquationType.QuadEaseOut;
        }

        Shape _particleShape;

        public void SetShape (Shape shape)
        {
            _particleShape = shape;
        }

        public override void SetAlpha(byte newAlpha)
        {
            Color col = _particleShape.FillColor;
            col.A = newAlpha;
            _particleShape.FillColor = col;
        }

        public override void Draw(SFML.Graphics.RenderWindow rw)
        {
            rw.Draw(_particleShape);
        }

        protected override void DoUpdate(float deltaT)
        {
            _particleShape.Position = Position;
        }
    }
}
