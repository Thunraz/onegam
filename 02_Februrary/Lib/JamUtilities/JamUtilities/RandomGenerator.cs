﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Window;

namespace JamUtilities
{
    public static class RandomGenerator
    {
        static public Random Random { get { if (_random == null) { _random = new Random(); } return _random; } }
        static private Random _random;

        static public Vector2f GetRandomVector2fSquare (float max)
        {
            return new Vector2f((float)(Random.NextDouble() - 0.5f) * 2.0f * max, (float)(Random.NextDouble() - 0.5f) * 2.0f * max);
        }
        static public Vector2f GetRandomVector2f(Vector2f xrange, Vector2f yrange)
        {
            CheckRanges(ref xrange);
            CheckRanges(ref yrange);

            float xDistance = xrange.Y - xrange.X;
            float yDistance = yrange.Y - yrange.X;

            return new Vector2f(((float)Random.NextDouble() * xDistance) + xrange.X, ((float)Random.NextDouble() * yDistance) + yrange.X);
        }

        static public Vector2f GetRandomVector2fInCircle (float radius)
        {
            return new Vector2f((float)(Math.Cos(Random.NextDouble()*2*Math.PI)*Random.NextDouble()*radius),(float)(Math.Sin(Random.NextDouble()*2*Math.PI)*Random.NextDouble()*radius));
        }
        static public Vector2f GetRandomVector2fOnCircle (float radius)
        {
            return new Vector2f((float)Math.Cos(Random.NextDouble()*2*Math.PI)*radius,(float)Math.Sin(Random.NextDouble()*2*Math.PI)*radius);
        }
        
	static private void CheckRanges (ref Vector2f range)
        {
            //if (range == null)
            //{
            //    throw new ArgumentNullException("range", "To create Random Numbers therange must exist." );
            //}
            if (range.X == range.Y)
            {
                throw new ArgumentOutOfRangeException("range", "To create Random Numbers the range must be existing.");
            }

            if (range.X > range.Y)
            {
                float tmp = range.X;
                range.X = range.Y;
                range.Y = tmp;
            }
        }
    }
}
