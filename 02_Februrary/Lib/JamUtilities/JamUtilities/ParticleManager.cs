﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.Window;

namespace JamUtilities
{
    public static class ParticleManager
    {


        private static System.Collections.Generic.List<IParticle> _particleList;

        public static uint NumberOfSmokeCloudParticles = 50;

        private static void CheckInitialized()
        {
            if (_particleList == null)
            {
                _particleList = new List<IParticle>();
            }
        }

        public static void Update (float deltaT)
        {
            CheckInitialized();
            System.Collections.Generic.List<IParticle> newList = new List<IParticle>();
            foreach (var p in _particleList)
            {
                p.Update(deltaT);
                if (p.IsAlive)
                {
                    newList.Add(p);
                }
            }

            _particleList = newList;
            Console.WriteLine(_particleList.Count);
        }

       

        public static void Draw (RenderWindow rw)
        {
            CheckInitialized();
            foreach (var p in _particleList)
            {
                p.Draw(rw);
            }
        }

        public static void SpawnSmokePuff(Vector2f position, Vector2f velocity, Color col, float smokePuffSize, float lifeTime = 1.5f)
        {
            CheckInitialized();
            Shape newShape = new CircleShape(smokePuffSize);
            newShape.Origin = new Vector2f(smokePuffSize / 2.0f, smokePuffSize / 2.0f);
            Color newColor = col;
            newColor.A = 20;
            newShape.FillColor = newColor;

            ShapeParticle particle = new ShapeParticle(newShape, lifeTime * ((float)RandomGenerator.Random.NextDouble() + 0.5f), position, velocity);
            particle.FrictionCoefficient = 0.97f;
            _particleList.Add(particle);
        }

        public static void SpawnSmokeCloud(Vector2f position, float cloudSize ,float smokePuffSize, Color col, float lifeTime = 1.5f)
        {
            CheckInitialized();
            for (uint i = 0; i != NumberOfSmokeCloudParticles; i++)
            {
                SpawnSmokePuff(position + RandomGenerator.GetRandomVector2fSquare(cloudSize), RandomGenerator.GetRandomVector2fSquare(50), col, smokePuffSize, lifeTime);
            }

        }

        //public static void SpawnExplosion

        public static void ResetParticleSystem()
        {
            _particleList = new List<IParticle>();
        }


    }
}
