﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.Window;

namespace JamUtilities
{
    public class AreatricCloud
    {

        private System.Collections.Generic.List<Vector2f> _offsetList;
        public static uint NumberOfCloudParticles = 250;
        private float _cloudRadius;
        private float _puffsize;
        private Color _cloudColor;
        public Vector2f Position { get; set; }
        private CircleShape circ;

        public AreatricCloud (Vector2f position, Color col, float puffSize = 7, float cloudRadius = 35)
        {

            _offsetList = new List<Vector2f>();
            _puffsize = puffSize;
            _cloudRadius = cloudRadius;
            _cloudColor = col;
            Position = position;
            circ = new CircleShape(_puffsize);
            circ.FillColor = _cloudColor;
            for (uint i = 0; i != NumberOfCloudParticles; i++)
            {
                //CircleShape newShape = new CircleShape(puffSize);
                //Color newColor = col;
                //newColor.A = 7;
                //newShape.FillColor = newColor;

                Vector2f offset = RandomGenerator.GetRandomVector2fInCircle(cloudRadius);
                _offsetList.Add(offset);
            }
        }

        public void Draw(RenderWindow rw)
        {
            foreach (var o in _offsetList)
            {
                circ.Position = Position + o;
                rw.Draw(circ);
            }
        }
    }
}
