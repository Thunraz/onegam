﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.Window;

namespace JamUtilities
{
    public static class ScreenEffects
    {

        private static float _totalTimeElapsed;

        private static Vector2u _screenSize;


        public static Color _fadeColor = Color.Black;
        private static Texture _fadeUpTexture;
        private static Sprite _fadeUpSprite;
        private static Texture _fadeDownTexture;
        private static Sprite _fadeDownSprite;

        private static Texture _fadeRadialTexture;
        private static Sprite _fadeRadialSprite;

        public static bool IsInScreenFlash { get; private set; }
        private static Color _screenFlashColor;
        private static float _screenFlashRemainingTime;
        private static float _screenFlashTotalTime;
        private static Texture _screenFlashTexture; 
        private static Sprite _screenFlashSprite;
        private static double _screenFlashInitialAlpha;


        public static bool IsInShake { get; private set; }
        private static float _remainingShakeTime;
        private static float _shakeTimer;
        private static float _shakeTimerMax;
        private static float _shakePower;
        private static ShakeDirection _shakeDirection;
        
        public static Vector2f GlobalSpriteOffset { get; private set; }

        public static void Init (Vector2u screenSize)
        {
            _totalTimeElapsed = 0.0f;
            _screenSize = screenSize;

	    GlobalSpriteOffset = new Vector2f(0.0f, 0.0f);

            CreateFadeSprites();

            Image screenFlashImage = new Image(_screenSize.X, _screenSize.Y, Color.White);
            _screenFlashTexture = new Texture(screenFlashImage);
            _screenFlashSprite = new Sprite(_screenFlashTexture);
        }

        

        private static void CreateFadeSprites()
        {
            Vector2u centerPosition = new Vector2u(_screenSize.X / 2, _screenSize.Y / 2);

            float distanceToCenterMax = (float)Math.Sqrt(centerPosition.X * centerPosition.X + centerPosition.Y * centerPosition.Y);

            Image fadeUpImage = new Image(_screenSize.X, _screenSize.Y);
            Image fadeDownImage = new Image(_screenSize.X, _screenSize.Y);
            Image fadeRadialImage = new Image(_screenSize.X, _screenSize.Y);
            
            for (uint i = 0; i != _screenSize.X; i++)
            {
                for (uint j = 0; j != _screenSize.Y; j++)
                {
                    Color newCol = _fadeColor;
                    float newAlpha = 255.0f * 0.35f * (float)(_screenSize.Y - j) / (float)(_screenSize.Y) + 0.0f;
                    newCol.A = (byte)newAlpha;
                    fadeUpImage.SetPixel(i, j, newCol);

                    newAlpha = 255.0f * 0.25f * (float)(j) / (float)(_screenSize.Y) + 0.0f;
                    newCol.A = (byte)newAlpha;
                    fadeDownImage.SetPixel(i, j, newCol);

                    Vector2u distanceToCenter = new Vector2u(centerPosition.X - i, centerPosition.Y - j);
                    newAlpha = 255.0f * 0.6f* (float)Math.Sqrt(distanceToCenter.X * distanceToCenter.X + distanceToCenter.Y * distanceToCenter.Y) /distanceToCenterMax;
                    newCol.A = (byte)newAlpha;
                    fadeRadialImage.SetPixel(i, j, newCol);
                }
            }
            _fadeUpTexture = new Texture(fadeUpImage);
            _fadeUpSprite = new Sprite(_fadeUpTexture);

            _fadeDownTexture = new Texture(fadeDownImage);
            _fadeDownSprite = new Sprite(_fadeDownTexture);

            _fadeRadialTexture = new Texture(fadeRadialImage);
            _fadeRadialSprite = new Sprite(_fadeRadialTexture);
            _fadeRadialSprite.Scale = new Vector2f(1.0f, 1.5f);

            
        }


        public static void Update (float deltaT)
        {
            _totalTimeElapsed += deltaT;
            if (IsInScreenFlash)
            {
                _screenFlashRemainingTime -= deltaT;
                if (_screenFlashRemainingTime <= 0.0f)
                {
                    IsInScreenFlash = false;
                    _screenFlashRemainingTime = 0.0f;
                }
            }

            if (IsInShake)
            {
                _shakeTimer -= deltaT;
                if (_shakeTimer <= 0.0f)
                {
                    if (_shakeDirection == ShakeDirection.AllDirections)
                    {
                        GlobalSpriteOffset = RandomGenerator.GetRandomVector2fSquare(_shakePower);
                    }
                    else if (_shakeDirection == ShakeDirection.UpDown)
                    {
                        GlobalSpriteOffset = new Vector2f(0.0f, (float)(RandomGenerator.Random.NextDouble() - 0.5f) * 2.0f * _shakePower);
                    }
                    else if (_shakeDirection == ShakeDirection.UpDown)
                    {
                        GlobalSpriteOffset = new Vector2f((float)(RandomGenerator.Random.NextDouble() - 0.5f) * 2.0f * _shakePower, 0.0f);
                    }
                    _shakeTimer = _shakeTimerMax;
                } 

                _remainingShakeTime-= deltaT;
                if(_remainingShakeTime <= 0.0f)
                {
                    GlobalSpriteOffset = new Vector2f(0.0f, 0.0f);
                    IsInShake = false;
                }
            }
        }


        public static void ScreenFlash(Color col, float duration)
        {
            _screenFlashColor = col;
            _screenFlashInitialAlpha = col.A;
            _screenFlashRemainingTime = _screenFlashTotalTime = duration;
            IsInScreenFlash = true;
        }

        public static void ShakeScreen (float duration, float shakeTime, float power, ShakeDirection shakeDirection = ShakeDirection.AllDirections)
        {
            if (duration < 0.0f)
            {
                throw new ArgumentOutOfRangeException("duration", duration, "Duration for a shake must be non-negative");
            }
            if (shakeTime< 0.0f)
            {
                throw new ArgumentOutOfRangeException("shakeTime", shakeTime, "Time for a shake must be non-negative");
            }

            IsInShake = true;
            _remainingShakeTime = duration;
            _shakePower = power;
            _shakeTimer = 0.0f;
            _shakeTimerMax = shakeTime;
            _shakeDirection = shakeDirection;
        }

        public static void DrawFadeUp (RenderWindow rw)
        {
            rw.Draw(_fadeUpSprite);
        }

        public static void DrawFadeDown(RenderWindow rw)
        {
            rw.Draw(_fadeDownSprite);
        }
        public static void DrawFadeRadial(RenderWindow rw)
        {
            rw.Draw(_fadeRadialSprite);
        }

        public static void Draw (RenderWindow rw)
        {
            if (IsInScreenFlash)
            {
                Color oldColor = _screenFlashSprite.Color;
                Color col = _screenFlashColor;
                col.A = (byte)(_screenFlashInitialAlpha * Math.Pow((_screenFlashRemainingTime / _screenFlashTotalTime),2));
                _screenFlashSprite.Color = col;

                rw.Draw(_screenFlashSprite);


            }
        }

        public static void ResetScreenEffects ()
        {
            IsInScreenFlash = false;
            IsInShake = false;

            _screenFlashRemainingTime = 0.0f;
            _remainingShakeTime = 0.0f;
        }


    }



    
}
