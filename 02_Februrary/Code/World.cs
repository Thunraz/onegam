﻿using SFML.Graphics;
using System;

namespace JamTemplate
{
    class World
    {

        #region Fields

        #endregion Fields

        #region Methods

        public World()
        {
            InitGame();
        }

        public void GetInput()
        {

        }

        public void Update(float deltaT)
        {

        }

        public void Draw(RenderWindow rw)
        {

        }

        private void InitGame()
        {
        }

        #endregion Methods

    }
}
